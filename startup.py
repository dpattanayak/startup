'''

Date    :   01.11.2020
Author  :   Debidatta Pattanayak
Purpose :   Boostup Productivity
Command :   pyinstaller --onefile -w 'startup.py'

'''

import datetime
import os
import subprocess
import webbrowser 

x = datetime.datetime.now()

date = x.strftime("%d.%m.%y")       #05.11.20
month = x.strftime("%b")            #Nov
year = x.strftime("%Y")             #2020
today = x.strftime("%a")            #Sun

directory = month + '-' + year
fileName = date + '.todo'
mileStone = 'milestone.todo'

pwd = '/home/clofus/Documents/Projects/todo'
ls = os.listdir(pwd)
path = os.path.join(pwd, directory)

# Disable the keyboard
subprocess.run(["xinput", "float", "12"])

if today != 'Sun':

    webbrowser.open('https://app.improductive.com')

    if directory in ls:
        # make only file

        cd = os.chdir(path)
        
        folder = os.listdir(os.getcwd())

        if fileName in folder:
            subprocess.run(["code", '/home/clofus/Documents/Projects/todo'])
        else:
            f = open(fileName, "a")
            f.write("Laptop Keyboard is disabled because it's not working properly. Run `xinput reattach 12 3` on terminal to enable")
            f.close()
            subprocess.run(["code", '/home/clofus/Documents/Projects/todo'])
    
    else:
        # for make a foler and file
        
        os.mkdir(path)
        cd = os.chdir(path)

        f = open(mileStone, "a")
        f.write("Clear this line before start this month's milestone TODO refer README opened in Files")
        f.close()

        f = open(fileName, "a")
        f.write("Laptop Keyboard is disabled because it's not working properly. Run `xinput reattach 12 3` on terminal to enable")
        f.close()
        subprocess.run(["code", '/home/clofus/Documents/Projects/todo'])
else:

    webbrowser.open('https://www.youtube.com/playlist?list=PLC3y8-rFHvwhBRAgFinJR8KHIrCdTkZcZ')